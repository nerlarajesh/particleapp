var Particle = require('particle-api-js');
var particle = new Particle();
var token, devicesPr;
var express = require('express');
var exphbs = require('express-handlebars');
var bodyParser = require('body-parser');
var app = express();
/*START:mongoDB connection and schema initialization*/
var MongoClient = require('mongodb').MongoClient;
var dburl = "mongodb://172.24.162.155:27017/particleapp";
/*END:mongoDB connection and schema initialization*/
app.use(express.static('public'));
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.set('view engine', 'handlebars');
app.get('/', function(request, response) {
    response.render('home');
});
app.post('/login', function(request, response) {
    if (typeof request.body.username != "undefined" && typeof request.body.pwd != "undefined") {
        particle.login({ username: request.body.username, password: request.body.pwd }).then(
            function(data) {
                token = data.body.access_token;
                devicesPr = particle.listDevices({ auth: data.body.access_token });
                response.render('landingpage');
            },
            function(err) {
                console.log('Could not log in.', err);
            }
        );
    }
});
app.get('/getData', function(request, response) {
    if (typeof devicesPr != "undefined") {
        devicesPr.then(
            function(devices) {
                var Devices = devices.body;
                Devices.forEach(function(element) {
                    if (element.connected) {
                        particle.getEventStream({ deviceId: element.id, auth: token }).then(function(stream) {
                            stream.on('event', function(data) {
                                console.log(data.data);
                                MongoClient.connect(dburl, function(err, db) {
                                    if (err) { throw err; }
                                    var collection = db.collection('particles');
                                    var particleData = { particle_id: element.id, publishedat: new Date(data.published_at).toLocaleTimeString(), publishedon: new Date(data.published_at).toLocaleDateString(), ttl: data.ttl, censor1_distance: data.data.split(',')[0].split(':')[1], censor1_time: data.data.split(',')[1].split(':')[1], censor2_distance: data.data.split(',')[2].split(':')[1], censor2_time: data.data.split(',')[3].split(':')[1] };
                                    collection.insert(particleData, function(err, result) {
                                        if (err) { throw err; }
                                        if (result)
                                            console.log("data inserted successfully")
                                        db.close();

                                    });

                                });
                                /*END:Inserting Event Data into MongoDB*/
                            });
                        });
                    } else {
                        console.log('None of the device connected');
                    }
                });
            },
            function(err) {
                console.log('List devices call failed: ', err);
            }
        );
    }
});
app.get('/paintChart', function(req, res) {
    MongoClient.connect(dburl, function(err, db) {
        db.collection('particles').find().toArray(function(e, data) {
            res.render('charts', { chartData: data });
            db.close();
        });

    });
});
app.listen(2000, function() {
    console.log('server started and running successfully');
});